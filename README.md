# README #

Di seguito i passi necessari per installare l'applicazione Sasso-Carta-Forbice sulla propria macchina.

* Scaricare e Installare [GIT](http://git-scm.com/download/win) .
* Scaricare e installare [NODE.JS](http://nodejs.org/) .
* Scaricare dal repository Sasso-Carta-Forbice:
       * Aprire il Promp dei comandi 
       * Posizionarsi nella cartella desiderata **cd move/to/your/path** 
       * Digitare **git clone https://magiceddy@bitbucket.org/magiceddy/rock-paper-scissors.git** 

* Installare tutte le dipendenze:
       * Posizionarsi nella cartella creata **cd move/to/your/path/rock-paper-scissors** 
       * Digitare **npm install**  

* Far partire l'app:  
       * Digitare **node server.js**
       * Aprire il Bowser e andare all'indirizzo **http://localhost:8080**