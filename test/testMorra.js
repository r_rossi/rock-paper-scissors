'use strict';

describe(" Player ",function(){
	var player;

	beforeEach(function(){
		player = new Player();
	});

	it(" set Player name as Computer ",function(){
		expect(player.name).toMatch("Computer");
	});

	it(" set score to zero ",function(){
		expect(player.score).toEqual(0);
	});

	it(" set choose to zero ",function(){
		expect(player.choose).toEqual(0);
	});

	it(" return name as Computer ",function(){
		expect(player.getName()).toMatch("Computer");
	});

	it(" set +1 to score",function(){
		var score = player.getScore();
		player.addScore();
		var score2 = player.getScore();
		expect(score2-score).toEqual(1);
	});

	it( "return player choose",function(){
		expect(player.choose).toEqual(player.getChoose());
	});

	it( "set choose between 1 and 3",function(){
		player.setChoose();
		expect(player.choose).toBeGreaterThan(0);
		expect(player.choose).toBeLessThan(4);
	});
});

describe("Human Player",function(){
	var humanPlayer;
	var human = "human";
	var errorHuman;

	beforeEach(function(){
		humanPlayer = new HumanPlayer(human);
		//errorHuman = new HumanPlayer();
	});

	it( "set pass name",function(){
		expect(humanPlayer.name).toMatch(human);
	});

	it( " set choose correct",function(){
		humanPlayer.setChoose(3);
		expect(humanPlayer.choose).toBeGreaterThan(0);
		expect(humanPlayer.choose).toBeLessThan(4);
	});

	it(" set a wrong hand",function(){
		humanPlayer.setChoose(5);
		expect(humanPlayer.choose).toEqual(0);
	});


	/*it( "throw error for null name value",function(){
		
		expect(function(){errorHuman = new HumanPlayer();}).toThrowError();
	});*/


});


describe("Game",function(){
		var game;
		var human;
		var computer;

		beforeEach(function(){
			human = new HumanPlayer("human");
			computer = new Player();
			game = new Game(human,computer);
		});

		it ( "verify the istance of pass Object", function(){
			expect(human).toEqual(jasmine.any(HumanPlayer));
		});

		it ( "verify the istance of pass Object", function(){
			expect(computer).toEqual(jasmine.any(Player));
		});
});