'use strict';

/**
* MorraCinese Module
'ngAnimate',
'ui.bootrap',
'ui.router'
*
* Description module that load the config file for route inside the app page end controller
*/
angular.module('MorraCinese',[
	'ngAnimate',
	'ui.bootstrap',
	'ui.router',
])
.config( function( $stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise( '/home' );
	$stateProvider
	.state('home', {
		url : '/home',
		templateUrl : 'index.html',
		controller : 'mainController'
	});
})
.controller('mainController', ['$scope','$animate','$state', function($scope,$animate,$state){
	

	$scope.icons = [{
		paper : "img/paper.png",
		scissor : "img/scissor.png",
		rock : "img/rock.png",
		wait : "img/wait.png"
	}];


	var computer = new Player();
	var human = new HumanPlayer('Human');
	var game = new Game(human, computer);

	$scope.humanName = human.getName();
	$scope.computerName = computer.getName();
	$scope.humanScore = human.getScore();
	$scope.computerScore = computer.getScore();
	$scope.humanChoose = 0;
	$scope.computerChoose = 0;

	$scope.setGame = function (humanChoose){
		
		//define human e computer choose
		human.setChoose(humanChoose);
		computer.setChoose();

		//after define human e computer choose, set the game.
		game.play();
		
		$scope.humanScore = human.getScore();
		$scope.computerScore = computer.getScore();
		$scope.humanChoose = human.getChoose();
		$scope.computerChoose = computer.getChoose();
	}


	$scope.playGame = function(){}
}]);