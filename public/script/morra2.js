function extend ( Child, Parent ){
	var App = function () {};
	App.prototype = Parent.prototype;
	Child.prototype = new App();
	Child.prototype.constructor = Child;
};

function Player(){

	this.setHand = function(callback){
		this.hand = Math.floor( ( Math.random() * 3 ) + 1 );

		if(this.hand < 0 || this.hand > 3 ) throw "Scelta computer non andata a buon fine";
		if(err) return callback (err,null);

		console.log("Il computer ha scelto: "+ this.hand);
		done = true;
		callback(err,true);
	};
};

Player.prototype.name = 'Computer';
Player.prototype.score = 0;
Player.prototype.hand = 0;
Player.prototype.getName = function(callback){
	var err = false;
	try{
		if(this.name === undefined) throw "Il computer non ha un nome!";
	}catch(err){
		return callback(err,null);
	}

	callback(err, this.name);
};
Player.prototype.getScore = function(callback){
	err = false;
	if (this.score === undefined) throw "Strano il computer sembra non avere nessun punteggio!";
	if(err) return callback(err,null);
	callback(err,this.score);
};
Player.prototype.addScore = function(callback){
	var err= false;
	var prevScore = this.score;
	++this.score;

	if(prevScore-this.score == 0) throw "Non è stato aggiunto nessun punteggio";
	if(err) return callback (err,null);
	callback(err,true);
};
Player.prototype.toString = function(){
	return this.name + "ha un punteggio pari a: " + this.score; 
};
Player.prototype.getHand = function(callback){
	err=false;
	if(this.hand === undefined) throw "C'è un problema con il punteggio gel giocatore";
	if(err) return callback(err,null);
	callback(err,this.hand);
}

function HumanPlayer(name,callback){
	var err = false;

	try{
		if(name === undefined) throw "Un umano deve avere un nome";
	}catch(err){
		return callback(err,null);
	}
	callback(err,true);

	this.setHand = function(hand,callback){
		var err = false;

		try{
			if(hand <1 || hand > 3)  throw "Devi inserire un valore compreso tra 1 e 3";
		}catch(err){
			return callback(err,null);
		}
		callback(err,true);
	};
};

extend(HumanPlayer,Player);

function Game(player1,player2,callback){
	var err = false;
	try{
		if ((((player1 instanceof HumanPlayer) || (player1 instanceof Player)) && ((player2 instanceof HumanPlayer) || (player2 instanceof Player))) === false)
			throw "Devi inserire due giocatori";
	}catch(err){
		return callback (err,null);
	}

	(player1 instanceof HumanPlayer) ? this.humanPlayer = player1 : this.computerPlayer = player1;
	(player2 instanceof HumanPlayer) ? this.humanPlayer = player2 : this.computerPlayer = player2;
		
	this.moves = {
		"2":"1",
		"3":"2",
		"1":"3"
	};

	this.play = function(callback){
		err=false;
		var humanHand;
		var computerHand;
		var err;

		this.humanPlayer.getHand(function(err,hand){
			if(err) return callback(err,null);
			humanHand = hand;
		});

		this.computerPlayer.getHand(function(err,hand){
			if(err) return callback(err,null);
			computerHand = hand;
		});

		if(humanHand != computerHand)
			(this.moves[humanHand] == computerHand) ? this.humanPlayer.addScore() : this.computerPlayer.addScore();
	}
};

	


