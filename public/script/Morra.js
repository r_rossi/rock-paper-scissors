//inheritance helper
function extend ( Child, Parent ){
	var App = function () {};
	App.prototype = Parent.prototype;
	Child.prototype = new App();
	Child.prototype.constructor = Child;
};

//Define Parent, in this case Computer Player
function Player (){
	// Define his own method setChoose
	this.setChoose = function(){
		this.choose = Math.floor( ( Math.random() * 3) + 1);
	};
};

/***************Define Player prototype (propertys and metods). *************/

//Player name
Player.prototype.name = 'Computer';
//Player Score
Player.prototype.score = 0;
//Player choose
Player.prototype.choose = 0;
//Return Player name
Player.prototype.getName = function(){
	return this.name;
};
//Return Player Score
Player.prototype.getScore = function(){
	return this.score;
};
// Add one point when win
Player.prototype.addScore = function(){
	++ this.score;
};
// Return player choose
Player.prototype.getChoose = function(){
	return this.choose;
};
// Return player name and score
Player.prototype.toString = function(){
	return this.name + " have: " + this.score + "points";	
};

/******************************End Player************************************/

//Define Human Player
function HumanPlayer( name ){
	//Human Player name
	this.name = name;
	try{
		if(this.name === undefined) throw "Human maust have a name!";
	}catch(err){
		console.log("Error: "+ err);
	}

	//Define Human Player Choose
	this.setChoose = function ( hand ){
		error=false;
		try{
			if(hand == 0 || hand > 3) throw "La mano inserita non è corretta"
		}catch(err){
			error=true;
			console.log("Error: "+err);
		}

		if(!error){
			this.choose = hand;
		}
	};
};

extend(HumanPlayer,Player);

//Define Game. Every game have 2 Player
function Game( player1, player2){
	try{
		if ((((player1 instanceof HumanPlayer) || (player1 instanceof Player)) && ((player2 instanceof HumanPlayer) || (player2 instanceof Player))) === false)
		throw "Devi inserire due giocatori!";
	}catch(err){
		console.log("Error: "+err);
	}
	
	
	(player1 instanceof HumanPlayer) ? this.humanPlayer = player1 : this.computerPlayer = player1;
	(player2 instanceof HumanPlayer) ? this.humanPlayer = player2 : this.computerPlayer = player2;
		
	// define possible choose
	this.moves = {
		"2" : "1",
		"3" : "2",
		"1" : "3"
	};

	//Method that start one hand
	this.play = function (){
		if(this.humanPlayer.getChoose() != this.computerPlayer.getChoose())
			(this.moves[this.humanPlayer.getChoose()] == this.computerPlayer.getChoose()) ? this.humanPlayer.addScore() : this.computerPlayer.addScore();
	};
};

