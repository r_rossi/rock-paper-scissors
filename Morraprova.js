//inheritance helper
function extend ( Child, Parent ){
	var App = function () {};
	App.prototype = Parent.prototype;
	Child.prototype = new App();
	Child.prototype.constructor = Child;
};


function Player (){
	this.setHand = function(){
		this.hand = Math.floor( ( Math.random() * 3 ) + 1 );
	};
};
Player.prototype.name = 'Computer';
Player.prototype.score = 0;
Player.prototype.hand = 0;
Player.prototype.getName = function(){ return this.name; };
Player.prototype.getScore = function(){ return this.score; };
Player.prototype.addScore = function(){ ++ this.score; };
Player.prototype.toString = function(){
	return this.name + "ha un punteggio pari a: " + this.score;
};
Player.prototype.getHand= function(){ return this.hand; };

//Human Player

function HumanPlayer( name ){
	this.name = name;
	try{
		if (this.name === undefined) throw  " Un umano deve avere il nome ";
	} catch(err) {
		console.log("Error: " + err);
	}

	this.setHand = function( hand ){
		error=false;
		try{
			if(hand == 0 || hand > 3) throw "La mano inserita non è corretta"
		}catch(err){
			error=true;
			console.log("Error: "+err);
		}

		if(!error){
			this.hand = hand;
		}
		
	};
};

/*HumanPlayer.prototype.setHand = function( hand ){
	this.hand = hand;
};*/


extend(HumanPlayer,Player);



function Game(player1, player2){
	this.humanPlayer;
	this.computerPlayer;

	try{
		if ((((player1 instanceof HumanPlayer) || (player1 instanceof Player)) && ((player2 instanceof HumanPlayer) || (player2 instanceof Player))) === false)
		throw "Devi inserire due giocatori!";
	}catch(err){
		console.log("Error: "+err);
	}
	
	
	(player1 instanceof HumanPlayer) ? this.humanPlayer = player1 : this.computerPlayer = player1;
	(player2 instanceof HumanPlayer) ? this.humanPlayer = player2 : this.computerPlayer = player2;
		
	this.moves = {
		"2":"1",
		"3":"2",
		"1":"3"
	};

	this.play = function(){
		if(this.humanPlayer.getHand() != this.computerPlayer.getHand())
		(this.moves[this.humanPlayer.getHand()] == this.computerPlayer.getHand()) ? this.humanPlayer.addScore() : this.computerPlayer.addScore();
	}
}



var pippo=3;

var player1 = new Player();
var player2 = new HumanPlayer("Rinaldo");
console.log(player2 instanceof HumanPlayer);
var game = new Game(player2,player1);
console.log("eee"+player1.name);
game.play(player2.setHand(3),player1.setHand());
console.log("Player1 name"+player1.name);
console.log("Player2 name"+player2.name);
console.log("Player1 score1"+player1.getScore());
console.log("Player2 score2"+player2.getScore());
console.log("Player1 score1"+player1.getScore());
console.log("Player2 score2"+player2.getScore());
console.log("Player1 hand1"+player1.getHand());
console.log("Player2 hand2"+player2.getHand());
console.log("Player1 score1"+player1.getScore());
console.log("Player2 score2"+player2.getScore());
console.log(player1.toString());
console.log(player2.toString());
console.log((3===3)?"si":"no");

